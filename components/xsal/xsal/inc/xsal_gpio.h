
#ifndef __XSAL_GPIO__
#define __XSAL_GPIO__
#include <stdint.h>
/* gpio operations */

typedef enum {
    GPIO_INPUT, 			/*!< GPIO mode : input only                           */
    GPIO_OUTPUT, 			/*!< GPIO mode : output only mode                     */
} xsal_mode_t;

typedef struct __gpio_protocol {
	int32_t (*gpio_cfg_fun)(uint32_t gpio, xsal_mode_t mode);
	int32_t (*gpio_out_fun)(uint32_t gpio, uint8_t val);
	int32_t (*gpio_in_fun)(uint32_t gpio, uint8_t val);
}gpio_protocol;

void init_gpio(void);
#endif /* __XSAL_GPIO__ */