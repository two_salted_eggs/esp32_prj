
#ifndef __XSAL_LCD__
#define __XSAL_LCD__
#include <stdint.h>

#define LCD_SEND_CMD                0
#define LCD_SEND_DAT                1

#define LCD_SPI_CLK                 (40*1000*1000)
#define LCD_PIN_MISO                -1
#define LCD_PIN_MOSI                13
#define LCD_PIN_CLK                 14
#define LCD_PIN_CS                  26
#define LCD_PIN_DC                  27
#define LCD_PIN_RST                 12
#define LCD_PIN_BCKL                25

#define LCD_QUE_TRANS               5
#define LCD_MEM_TRANS               1024*4
#define LCD_MEM_TRANS_DMA           LCD_MEM_TRANS*2


/* LCD operations */
typedef struct __lcd_protocol {
	int (*lcd_init)(void);
	int (*lcd_reset)(void);
	int (*lcd_uninit)(void);
    int (*lcd_readid)(void);
    int (*lcd_blkctl)(unsigned char levl);
	int (*lcd_setregion)(int x_start, int y_start, int x_end, int y_end);
	int (*lcd_refresh)(unsigned char  *data, unsigned int size);
}lcd_protocol;

void init_lcd(void);
#endif /* __XSAL_LCD__ */

