#include "xsal.h"

static xsal_t *xsal_p[MAX_INFO_PROTOCOL];


int xsal_register(xsal_t *xsal)
{
    xsal_p[xsal->xsal_id] = xsal;
    return 0;
}

int xsal_get_register(XSAL_ID_E xsal_id, void **xsal_ops)
{
    *xsal_ops = xsal_p[xsal_id]->ops;
    return 0;
}

int xsal_init(void)
{
    init_gpio();
    init_lcd();
    return MAX_INFO_PROTOCOL;
}
