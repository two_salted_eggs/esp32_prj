
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "xsal_lcd.h"
#include "xsal_gpio.h"
#include "xsal.h"

static int lcd_reset(void);
static int lcd_blkctl(unsigned char levl);
/*
*   1.8�� ����7735S  
*   �ֱ��ʣ�128*160      
*/
static spi_device_handle_t  spi_device;

typedef struct {
    uint8_t cmd;
    uint8_t data[16];
    uint8_t databytes; //No of data in data; bit 7 = delay after set; 0xFF = end of cmds.
} lcd_init_cmd_t;

static lcd_init_cmd_t lcd_init_cmds[]={
    {0x11, {0}, 0x80},
    {0xB1, {0x01, 0x2C, 0x2D}, 3},
    {0xB2, {0x01, 0x2C, 0x2D}, 3},
    {0xB3, {0x01, 0x2C, 0x2D, 0x01, 0x2C, 0x2D}, 6},
    {0xB4, {0x07}, 1},
    {0xC0, {0xA2, 0x02, 0x84}, 3},
    {0xC1, {0xC5}, 1},
    {0xC2, {0x0A, 0x00}, 2},
    {0xC3, {0x8A, 0x2A}, 2},
    {0xC4, {0x8A, 0xEE}, 2},
    {0xC5, {0x0E}, 1},
    {0x36, {0xC0}, 1},
    {0xE0, {0x0f, 0x1a, 0x0f, 0x18, 0x2f, 0x28, 0x20, 0x22, 0x1f, 0x1b, 0x23, 0x37, 0x00, 0x07, 0x02, 0x10}, 16},
    {0XE1, {0x0f, 0x1b, 0x0f, 0x17, 0x33, 0x2c, 0x29, 0x2e, 0x30, 0x30, 0x39, 0x3f, 0x00, 0x07, 0x03, 0x10}, 16},
    {0x2A, {0x00, 0x00, 0x00, 0x80+2}, 4},
    {0x2B, {0x00, 0x00, 0x00, 0xA0+3}, 4},
    {0x3A, {0x05}, 1},
    {0x29, {0}, 0x80},
    {0xff, {0xff}, 0xff}
};

static void lcd_gpio_init(void)
{
    esp_err_t ret;
    gpio_pad_select_gpio(LCD_PIN_DC);
    gpio_pad_select_gpio(LCD_PIN_RST);
    gpio_pad_select_gpio(LCD_PIN_BCKL);
    //Initialize non-SPI GPIOs
    ret = gpio_set_direction(LCD_PIN_DC,   GPIO_MODE_OUTPUT);
    assert(ret==ESP_OK);
    ret = gpio_set_direction(LCD_PIN_RST,  GPIO_MODE_OUTPUT);
    assert(ret==ESP_OK);
    ret = gpio_set_direction(LCD_PIN_BCKL, GPIO_MODE_OUTPUT);
    assert(ret==ESP_OK);
    return;
}

static void lcd_spi_pre_transfer_callback(spi_transaction_t *t) 
{
    int dc=(int)t->user;
    gpio_set_level(LCD_PIN_DC, dc);
}

static void lcd_spi_init(void)
{
    esp_err_t ret;
    spi_bus_config_t buscfg={
        .miso_io_num=LCD_PIN_MISO,
        .mosi_io_num=LCD_PIN_MOSI,
        .sclk_io_num=LCD_PIN_CLK,
        .quadwp_io_num=-1,
        .quadhd_io_num=-1,
        .max_transfer_sz= LCD_MEM_TRANS_DMA,
    };

    spi_device_interface_config_t devcfg={      
        .clock_speed_hz = LCD_SPI_CLK,                  //Clock out at 26 MHz. Yes, that's heavily overclocked.
        .mode=0,                                        //SPI mode 0
        .queue_size=LCD_QUE_TRANS,                      //We want to be able to queue this many transfers
        .spics_io_num= LCD_PIN_CS,                      //CS pin
        .duty_cycle_pos = 0,
        .flags = SPI_DEVICE_3WIRE,
        .pre_cb=lcd_spi_pre_transfer_callback,          //Specify pre-transfer callback to handle D/C line
        .post_cb = NULL,
    };
    //Initialize the SPI bus
    ret=spi_bus_initialize(HSPI_HOST, &buscfg, SPI_DMA_CH_AUTO);
    assert(ret==ESP_OK);
    //Attach the LCD to the SPI bus
    ret=spi_bus_add_device(HSPI_HOST, &devcfg, &spi_device);
    assert(ret==ESP_OK);
    return;
}


void lcd_send_cmd(uint8_t cmd) 
{
    esp_err_t ret;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));           //Zero out the transaction
    t.length=8;                         //Command is 8 bits
    t.tx_buffer=&cmd;                   //The data is the cmd itself
    t.user= LCD_SEND_CMD;                    //D/C needs to be set to 0
    ret=spi_device_transmit(spi_device, &t);   //Transmit!
    assert(ret==ESP_OK);                //Should have had no issues.
}

void lcd_send_data(uint8_t *data, int len) 
{
    esp_err_t ret;
    spi_transaction_t t;
    if (len==0) return;             //no need to send anything
    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=len*8;                 //Len is in bytes, transaction length is in bits.
    t.tx_buffer=data;               //Data
    t.user= LCD_SEND_DAT;                //D/C needs to be set to 1
    ret=spi_device_transmit(spi_device, &t);  //Transmit!
    assert(ret==ESP_OK);            //Should have had no issues.
}


void lcd_device_init(void) 
{
    int cmd=0;
    //Reset the display
    lcd_reset();

    //Send all the commands
    while (lcd_init_cmds[cmd].cmd!=0xff) 
    {
        lcd_send_cmd(lcd_init_cmds[cmd].cmd);
        lcd_send_data(lcd_init_cmds[cmd].data, lcd_init_cmds[cmd].databytes&0x1F);
        if (lcd_init_cmds[cmd].databytes&0x80) 
        {
            vTaskDelay(100 / portTICK_RATE_MS);
        }
        cmd++;
    }
    gpio_set_level(LCD_PIN_BCKL, 1);
}



static int lcd_init(void)
{
    printf("start init lcd");
    lcd_blkctl(0);
    lcd_gpio_init();
    lcd_spi_init();
    lcd_device_init();
    return 0;
}

static int lcd_reset(void)
{
    gpio_set_level(LCD_PIN_RST, 0);
    vTaskDelay(100 / portTICK_RATE_MS);
    gpio_set_level(LCD_PIN_RST, 1);
    vTaskDelay(100 / portTICK_RATE_MS);
    return 0;
}


static int lcd_blkctl(unsigned char levl)
{
    esp_err_t ret;
    if (levl) {
        ret = gpio_set_level(LCD_PIN_BCKL, 1);
        assert(ret==ESP_OK);
    }
    else {
        ret = gpio_set_level(LCD_PIN_BCKL, 0);
        assert(ret==ESP_OK);
    }
    return 0;
}


static int lcd_setregion(int x_start, int y_start, int x_end, int y_end)
{
    
    unsigned char tx_data[4];


    lcd_send_cmd(0x2A);

    tx_data[0]=0;          //Start Col High
    tx_data[1]=x_start;    //Start Col Low
    tx_data[2]=0;          //End Col High
    tx_data[3]=x_end;      //End Col Low
    lcd_send_data(tx_data, 4);

    lcd_send_cmd(0x2B);

    tx_data[0]=0;          //Start page high
    tx_data[1]=y_start;    //start page low
    tx_data[2]=0;          //end page high
    tx_data[3]=y_end;      //end page low
    lcd_send_data(tx_data, 4);

    lcd_send_cmd(0x2C);
    return 0;
}

int lcd_refresh(unsigned char  *data, unsigned int size)
{
    
    #if 0
    lcd_send_data(data, size);
    #else
    unsigned int num = 0;
    unsigned int overlength = 0;
    if(size < LCD_MEM_TRANS)
    {
        lcd_send_data(data, size);
    }
    else
    {
        num = size/LCD_MEM_TRANS;
        overlength = size%LCD_MEM_TRANS;
        for(int i=0; i<num; i++)
        {
            lcd_send_data(data, LCD_MEM_TRANS);
            data = data+LCD_MEM_TRANS;
        }
        lcd_send_data(data, overlength);
    }
    #endif
    return 0;
}

lcd_protocol lcd_protocol_t = {
    .lcd_init = lcd_init,
    .lcd_reset = lcd_reset,
    .lcd_uninit = NULL,
    .lcd_readid = NULL,
    .lcd_blkctl = lcd_blkctl,
    .lcd_setregion = lcd_setregion,
    .lcd_refresh = lcd_refresh,
};

xsal_t xsal_lcd_t = 
{
    .xsal_id = LCD_7735S_PROTOCOL,
    .ops = &lcd_protocol_t,
    .status = 0,
};

void init_lcd(void)
{
    xsal_register(&xsal_lcd_t);
}
