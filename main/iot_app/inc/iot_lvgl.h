#ifndef __IOT_LVGL_H__
#define __IOT_LVGL_H__
#include "freertos/FreeRTOS.h"

#define LCD_BUF_SIZE                2048//(CONFIG_LV_HOR_RES_MAX * 40)

void lv_main(void *pvParameter);

#endif
