#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_freertos_hooks.h"
#include "freertos/semphr.h"
#include "esp_system.h"
#include "esp_log.h"

#include "xcmd.h"
#include "iot_app.h"
#include "iot_xcmd.h"
#include "iot_aliot.h"
#include "iot_lvgl.h"
#include "xcmd_task.h"
#define TAG "IOT_APP"


t_xcmd_task lv_task = {
	.p_task_desc.task_entry = lv_main,
	.p_task_desc.task_name = "lvgl",
	.p_task_desc.stackdepth = 1024*8,
	.p_task_desc.priority = 5,
};

t_xcmd_task aliot_task = {
	.p_task_desc.task_entry = aliot_main,
	.p_task_desc.task_name = "aliot",
	.p_task_desc.stackdepth = 1024*8,
	.p_task_desc.priority = 8,
};


void iot_init(void)
{
	ESP_LOGI(TAG, "IOT APP Start......");
	iot_xcmd_init();			//iot_app 相关得cmd注册
	xcmd_exec("xsal");			//初始化xsal
	xcmd_exec("nvs");			//初始化nvs
	//xcmd_exec("winit");		//wifi初始化
	//xcmd_exec("wscan");		//wifi扫描
	//xcmd_exec("wcon chucheng 12345678");		//连接wifi
	xcmd_tsak_register(&lv_task, 0);
	xcmd_tsak_register(&aliot_task, 0);
}