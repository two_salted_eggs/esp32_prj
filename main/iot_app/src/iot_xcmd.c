#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "xsal.h"
#include "xcmd.h"
#include "iot_xcmd.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"
#include "cJSON.h"
/* Littlevgl specific */
#include "lvgl.h"
#include "iot_aliot.h"
#include "eventsrv.h"
#include "esp_https_ota.h"
#define TAG "XCMD_APP"
static const char *HTTP_TAG = "http_task";

int xcmd_gui_txt(int argc, char* argv[])
{
    static lv_style_t style;
    static char flag = 1;
    static lv_obj_t * obj;
    char *str = NULL;

    if (argc == 1)
    {
        str = argv[0];
    }
    
    if (flag == 1)
    {
        lv_style_init(&style);
        /*Set a background color and a radius*/
        lv_style_set_radius(&style, LV_STATE_DEFAULT, 5);
        lv_style_set_bg_opa(&style, LV_STATE_DEFAULT, LV_OPA_COVER);
        lv_style_set_bg_color(&style, LV_STATE_DEFAULT, LV_COLOR_WHITE);
        /*Add a value text properties*/
        lv_style_set_value_color(&style, LV_STATE_DEFAULT, LV_COLOR_RED);
        lv_style_set_value_align(&style, LV_STATE_DEFAULT, LV_ALIGN_IN_TOP_LEFT);
        lv_style_set_value_ofs_x(&style, LV_STATE_DEFAULT, 10);
        lv_style_set_value_ofs_y(&style, LV_STATE_DEFAULT, 10);
        /*Create an object with the new style*/
        obj = lv_obj_create(lv_scr_act(), NULL);
        lv_obj_add_style(obj, LV_OBJ_PART_MAIN, &style);
        lv_obj_align(obj, NULL, LV_ALIGN_CENTER, 0, 0);
        flag = 0;
    }
    /*Add a value text to the local style. This way every object can have different text*/
    lv_obj_set_style_local_value_str(obj, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, str);
    return 0;
}

int xcmd_lcd_test(int argc, char* argv[])
{
    static unsigned char flg = 0;
    static lcd_protocol *lcd_ops;
    static unsigned char *pcolor;
    unsigned short color = 0xf800;
    unsigned int i=0;

    if(argc == 2)
    {
        color = atoi(argv[1]);
        printf("color = %d\r\n", color);
    }

    if(flg == 0)
    {
        xsal_get_register(LCD_7735S_PROTOCOL, (void *)&lcd_ops);
        lcd_ops->lcd_init();
        lcd_ops->lcd_setregion(0, 0, 127, 159);
        flg = 1;
    }
    
    pcolor=heap_caps_malloc(1024, MALLOC_CAP_DMA);
    assert(pcolor != NULL);
    for (int i=0; i<1024; i+=2) 
    {
        pcolor[i+0]=color>>8;
		pcolor[i+1]=color;
	}
    for(i=0; i<((128*160*2)/1024); i++)
    {
        lcd_ops->lcd_refresh(pcolor, 1024);
    }
    heap_caps_free(pcolor);
    return 0;
}

int xcmd_set_lcdbk(int argc, char* argv[])
{
    unsigned char red=100, green=100, blue=100;
    lv_disp_t disp;
    lv_color_t color = lv_color_make(red, green, blue);
    xcmd_print("color = %d\r\n", color);
    lv_disp_set_bg_color(&disp, color);
    return 0;
}

int xsal_init_fun(int argc, char* argv[])
{
    xcmd_print("xsal init start ...\r\n");
    xsal_init();
    xcmd_print("xsal init end ... \r\n");
    return 0;
} 



#define WEB_SERVER          "api.thinkpage.cn"              
#define WEB_PORT            "80"
#define WEB_URL             "/v3/weather/now.json?key="
#define APIKEY		        "g3egns3yk2ahzb0p"       
#define city		        "suzhou"
#define language	        "en"

static const char *REQUEST = "GET "WEB_URL""APIKEY"&location="city"&language="language" HTTP/1.1\r\n"
    "Host: "WEB_SERVER"\r\n"
    "Connection: close\r\n"
    "\r\n";

//天气解析结构体
typedef struct 
{
    char cit[20];
    char weather_text[20];
    char weather_code[2];
    char temperatur[3];
}weather_info;

weather_info weathe;
/*解析json数据 只处理 解析 城市 天气 天气代码  温度  其他的自行扩展
* @param[in]   text  		       :json字符串
* @retval      void                 :无
* @note        修改日志 
*               Ver0.0.1:
                    hx-zsj, 2018/08/10, 初始化版本\n 
*/
void cjson_to_struct_info(char *text)
{
    cJSON *root,*psub;
    cJSON *arrayItem;
    //截取有效json
    char *index=strchr(text,'{');
    strcpy(text,index);

    root = cJSON_Parse(text);
    
    if(root!=NULL)
    {
        psub = cJSON_GetObjectItem(root, "results");
        arrayItem = cJSON_GetArrayItem(psub,0);

        cJSON *locat = cJSON_GetObjectItem(arrayItem, "location");
        cJSON *now = cJSON_GetObjectItem(arrayItem, "now");
        if((locat!=NULL)&&(now!=NULL))
        {
            psub=cJSON_GetObjectItem(locat,"name");
            sprintf(weathe.cit,"%s",psub->valuestring);
            ESP_LOGI(HTTP_TAG,"city:%s",weathe.cit);

            psub=cJSON_GetObjectItem(now,"text");
            sprintf(weathe.weather_text,"%s",psub->valuestring);
            ESP_LOGI(HTTP_TAG,"weather:%s",weathe.weather_text);
            
            psub=cJSON_GetObjectItem(now,"code");
            sprintf(weathe.weather_code,"%s",psub->valuestring);
            ESP_LOGI(HTTP_TAG,"%s",weathe.weather_code);

            psub=cJSON_GetObjectItem(now,"temperature");
            sprintf(weathe.temperatur,"%s",psub->valuestring);
            ESP_LOGI(HTTP_TAG,"temperatur:%s",weathe.temperatur);

            ESP_LOGI(HTTP_TAG,"--->city %s,weather %s,temperature %s<---\r\n",weathe.cit,weathe.weather_text,weathe.temperatur);
        }
    }
    cJSON_Delete(root);
}

int http_get_weather(int argc, char* argv[])
{
    const struct addrinfo hints = {
        .ai_family = AF_INET,
        .ai_socktype = SOCK_STREAM,
    };
    struct addrinfo *res;
    int s, r;
    char recv_buf[1024];
    char mid_buf[1024];

    //DNS域名解析
    int err = getaddrinfo(WEB_SERVER, WEB_PORT, &hints, &res);
    if(err != 0 || res == NULL) 
    {
        ESP_LOGE(HTTP_TAG, "DNS lookup failed err=%d res=%p\r\n", err, res);
        return -1;
    }

    //新建socket
    s = socket(res->ai_family, res->ai_socktype, 0);
    if(s < 0) {
        ESP_LOGE(HTTP_TAG, "... Failed to allocate socket.\r\n");
        close(s);
        freeaddrinfo(res);
        return -1;
    }
    //连接ip
    if(connect(s, res->ai_addr, res->ai_addrlen) != 0) {
        ESP_LOGE(HTTP_TAG, "... socket connect failed errno=%d\r\n", errno);
        close(s);
        freeaddrinfo(res);
        return -1;
    }
    freeaddrinfo(res);
    //发送http包
    if (write(s, REQUEST, strlen(REQUEST)) < 0) {
        ESP_LOGE(HTTP_TAG, "... socket send failed\r\n");
        close(s);
        return -1;
    }
    //清缓存
    memset(mid_buf,0,sizeof(mid_buf));
    //获取http应答包
    do {
        bzero(recv_buf, sizeof(recv_buf));
        r = read(s, recv_buf, sizeof(recv_buf)-1);
        strcat(mid_buf,recv_buf);
    } while(r > 0);
    //json解析
    cjson_to_struct_info(mid_buf);
    //关闭socket，http是短连接
    close(s);
    return 0;
}

int xcmd_connect_aliot(int argc, char* argv[])
{
    aliot_main(0);
    return 0;
}

int xcmd_gpio_out(int argc, char* argv[])
{ 
    gpio_protocol  *gpio_ops;
    int gpio_num = 0;
    int gpio_val = 0;
    int time = 0;
    xsal_get_register(GPIO_INFO_PROTOCOL, (void *)&gpio_ops);

    if( argc == 4) 
    {
        gpio_num = atoi(argv[1]);
        gpio_val = atoi(argv[2]);
        time = atoi(argv[3]);
        xcmd_print("gpio_num %d,gpio_val %d,time %d\r\n", gpio_num, gpio_val, time);
        
    }
    else 
    {
        xcmd_print("error\r\n");
        return -1;
    }
    gpio_ops->gpio_cfg_fun(gpio_num, GPIO_OUTPUT);
    gpio_ops->gpio_out_fun(gpio_num, gpio_val);
    vTaskDelay(time / portTICK_PERIOD_MS);
    if(gpio_val == 0)
    {
        gpio_ops->gpio_out_fun(gpio_num, 1);
    }
    else
    {
        gpio_ops->gpio_out_fun(gpio_num, 0);
    }
    return 0;
}



extern const uint8_t server_cert_pem_start[] asm("_binary_ca_cert_pem_start");

esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
    switch (evt->event_id) {
    case HTTP_EVENT_ERROR:
        ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
        break;
    case HTTP_EVENT_ON_CONNECTED:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
        break;
    case HTTP_EVENT_HEADER_SENT:
        ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
        break;
    case HTTP_EVENT_ON_HEADER:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
        break;
    case HTTP_EVENT_ON_DATA:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
        break;
    case HTTP_EVENT_ON_FINISH:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
        break;
    case HTTP_EVENT_DISCONNECTED:
        ESP_LOGD(TAG, "HTTP_EVENT_DISCONNECTED");
        break;
    }
    return ESP_OK;
}

int xcmd_ota_http(int argc, char* argv[])
{
    static char url[128] = {0};
    ESP_LOGI(TAG, "Starting OTA example");

    esp_http_client_config_t config = {
        //.cert_pem = (char *)server_cert_pem_start,
        .event_handler = _http_event_handler,
        .keep_alive_enable = true,
    };

    if( argc == 2) 
    {
        memcpy(url, argv[1], strlen(argv[1]) );
    }
    else 
    {
        ESP_LOGE(TAG, "input param num error");
        return -1;
    }
    config.url = url;
    esp_err_t ret = esp_https_ota(&config);
    if (ret == ESP_OK) 
    {
        esp_restart();
    } else 
    {
        ESP_LOGE(TAG, "Firmware upgrade failed");
    }
    return 0;
}


int xcmd_event_test(int argc, char* argv[])
{
    int ret = 0;
    if( argc == 2) 
    {
        ret = xcmd_sendeventtoclient(KEYPAD_SERVICE, atoi(argv[1]), NULL);
        ESP_LOGE(TAG, "xcmd_sendeventtoclient %d", ret);
    }
    else
    {
        return -1;
    }
    return 0;
}

xcmd_t t_iot_xcmd_pci[] = 
{
    {"ltxt",        xcmd_gui_txt,               "display text",             NULL},      //lcd显示字符
    {"lbk",         xcmd_set_lcdbk,             "xcmd_set_lcdbk",           NULL},
    {"xsal",        xsal_init_fun,              "xsal init",                NULL},      
    {"http",        http_get_weather,           "http_get_weather",         NULL},      //获取天气
    {"aliot",       xcmd_connect_aliot,         "xcmd_connect_aliot",       NULL},
    {"gov",         xcmd_gpio_out,              "xcmd_gpio_out",            NULL},
    {"ota",         xcmd_ota_http,              "xcmd_ota_http",            NULL},      //ota升级
    //{"event",       xcmd_event_test,            "xcmd_event_test",          NULL},
};


void iot_xcmd_init(void)
{
    xcmd_cmd_register(t_iot_xcmd_pci, sizeof(t_iot_xcmd_pci)/sizeof(xcmd_t));
}
