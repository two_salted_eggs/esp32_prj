/* LVGL Example project
 *
 * Basic project to test LVGL on ESP32 based projects.
 *
 * This example code is in the Public Domain (or CC0 licensed, at your option.)
 *
 * Unless required by applicable law or agreed to in writing, this
 * software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 */
#include "stdio.h"
#include "sdkconfig.h"
#ifdef CONFIG_ESP_XCMD_EXTEND
#include "esp_xcmd.h"
#endif
#ifdef CONFIG_IOT_APP_CONFIG
#include "iot_app.h"
#endif

#define TAG "demo"

void app_main() 
{
    #ifdef CONFIG_ESP_XCMD_EXTEND
    esp_xcmd_init();
    #endif
    #ifdef CONFIG_IOT_APP_CONFIG
    iot_init();
    #endif
}
