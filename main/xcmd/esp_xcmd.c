#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "xcmd.h"
#include "esp_xcmd.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "esp_event.h"
#include "nvs_flash.h"
#include "xsal.h"

#include "esp_https_ota.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "xcmd_task.h"
#include "eventsrv.h"
#define TAG "XCMD_APP"

/* Set the SSID and Password via project configuration, or can set directly here */
#define DEFAULT_SSID            "Xiaomi_5E80"
#define DEFAULT_PWD             "qw12ty124"
#define DEFAULT_SCAN_METHOD     WIFI_FAST_SCAN
#define DEFAULT_SORT_METHOD     WIFI_CONNECT_AP_BY_SIGNAL
#define DEFAULT_RSSI            -127
#define DEFAULT_AUTHMODE        WIFI_AUTH_OPEN


static void event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
	ESP_LOGI(TAG, "event_handler, event_id:%d", event_id);
    if (event_base == WIFI_EVENT) 
    {
        switch (event_id)
		{
			case WIFI_EVENT_SCAN_DONE:			// 扫描AP完成
				ESP_LOGI(TAG, "event-->ESP32 finish scanning AP");
				break;
			case WIFI_EVENT_STA_START:			// 开始STA模式
				ESP_LOGI(TAG, "event-->ESP32 station start");
				esp_wifi_connect();
				break;
			case WIFI_EVENT_STA_STOP:				// 停止STA模式
				ESP_LOGI(TAG, "event-->ESP32 station stop");
				break;
			case WIFI_EVENT_STA_CONNECTED:		// STA模式连接到AP
				ESP_LOGI(TAG, "event-->ESP32 station connected to AP");
				break;
			case WIFI_EVENT_STA_DISCONNECTED:		// STA模式断开与AP的连接
				break;
			case WIFI_EVENT_STA_AUTHMODE_CHANGE:	// ESP32 Station连接的AP的验证模式已更改
				ESP_LOGI(TAG, "event-->the auth mode of AP connected by ESP32 station changed");
				break;
			case WIFI_EVENT_STA_WPS_ER_SUCCESS:	// ESP32 station wps在注册模式下成功
				ESP_LOGI(TAG, "event--> ESP32 station wps succeeds in enrollee mode");
				break;
			case WIFI_EVENT_STA_WPS_ER_FAILED:	// ESP32 station wps在注册模式下失败
				ESP_LOGI(TAG, "event--> ESP32 station wps fails in enrollee mode");
				break;
			case WIFI_EVENT_STA_WPS_ER_TIMEOUT:	// ESP32 station wps在注册模式超时
				ESP_LOGI(TAG, "event--> ESP32 station wps timeout in enrollee mode");
				break;
			case WIFI_EVENT_STA_WPS_ER_PIN:		// ESP32 Station WPS密码在登记人模式下
				ESP_LOGI(TAG, "event--> ESP32 station wps pin code in enrollee mode");
				break;
			case WIFI_EVENT_STA_WPS_ER_PBC_OVERLAP:// ESP32 station wps在登记人模式下重叠
				ESP_LOGI(TAG, "event--> ESP32 station wps overlap in enrollee mode");
				break;
			case WIFI_EVENT_AP_START:				// ESP32 AP模式开始
				ESP_LOGI(TAG, "event--> ESP32 soft-AP start");
				break;
			case WIFI_EVENT_AP_STOP:				// ESP32 AP模式停止
				ESP_LOGI(TAG, "event--> ESP32 soft-AP stop");
				break;
			case WIFI_EVENT_AP_STACONNECTED:		// ESP32 AP模式下，有站接入此AP
				ESP_LOGI(TAG, "event--> a station connected to ESP32 soft-AP ");
				break;
			case WIFI_EVENT_AP_STADISCONNECTED:		// ESP32 AP模式下，有站断开此AP
				ESP_LOGI(TAG, "event--> a station disconnected from ESP32 soft-AP ");
				break;
			case WIFI_EVENT_AP_PROBEREQRECVED:		// ESP32 AP模式下，在soft-AP接口中接收探测请求数据包
				ESP_LOGI(TAG, "event--> Receive probe request packet in soft-AP interface");
				break;
			default:
				break;
		}
    } 
    
	if (event_base == IP_EVENT) 
    {
		switch (event_id)
		{
			case IP_EVENT_STA_GOT_IP:			// ESP32 Station从连接的AP获取到IP
				ESP_LOGI(TAG, "event-->ESP32 station got IP from connected AP");
				ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
    	    	ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
				break;		
			case IP_EVENT_STA_LOST_IP:			// ESP32 Station丢失IP或IP重置
				ESP_LOGI(TAG, "event-->ESP32 station lost IP and the IP is reset to 0");
				break;
			case IP_EVENT_AP_STAIPASSIGNED:		// ESP32 AP模式下，为连接的站分配IP
				ESP_LOGI(TAG, "event--> ESP32 soft-AP assign an IP to a connected station");
				break;
		}
    }
}

int wifi_init(int argc, char* argv[])
{
    if(esp_wifi_start() != ESP_ERR_WIFI_NOT_INIT)
    {
        ESP_LOGI(TAG, "Wifi already initialization is complete");
        return -1;
    }
	//Wi-Fi/LwIP 初始化阶段
	ESP_ERROR_CHECK(esp_netif_init());	// 初始化底层TCP/IP堆栈。在应用程序启动时，应该调用此函数一次
	ESP_ERROR_CHECK(esp_event_loop_create_default());				// 创建默认事件循环
	esp_netif_t *sta_netif = esp_netif_create_default_wifi_sta();   // 创建一个默认的WIFI-STA网络接口
    assert(sta_netif);

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();    
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));                           // 使用默认wifi初始化配置

	esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID,   	&event_handler, NULL, &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT, 	IP_EVENT_STA_GOT_IP,  	&event_handler, NULL, &instance_got_ip));
    return 0;
}


esp_netif_t *sta_netif;
int xcmd_wifi_connect(int argc, char* argv[])
{
    char *ssid;
    char *password;
    //Wi-Fi 配置阶段
    wifi_config_t wifi_config = {
        .sta = {
            .scan_method = DEFAULT_SCAN_METHOD,
            .sort_method = DEFAULT_SORT_METHOD,
            .threshold.rssi = DEFAULT_RSSI,
            .threshold.authmode = DEFAULT_AUTHMODE,
        },
    };

    if( argc == 3) 
    {
        ssid = argv[1];
        password = argv[2];
        if(esp_wifi_start() == ESP_OK)
        {
            xcmd_exec("wdisc");
        }
    }
    else 
    {
        if(esp_wifi_start() == ESP_OK)
        {
            xcmd_print("Wifi already connect!\n");
            return 0;
        }
        ssid = DEFAULT_SSID;
        password = DEFAULT_PWD;
    }
    

    memcpy(wifi_config.sta.ssid, ssid, strlen(ssid));
    memcpy(wifi_config.sta.password, password, strlen(password));
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));              //将 Wi-Fi 模式配置为 station 模式
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    xcmd_print("Wifi configuration information \n");
    xcmd_print("SSID: %s\n", wifi_config.sta.ssid);
    xcmd_print("PASSWORD: %s\n", wifi_config.sta.password);

    //Wi-Fi 启动阶段
    ESP_ERROR_CHECK(esp_wifi_start());

    //4.Wi-Fi 连接阶段  
    //事件回调函数中进行
    //其余阶段处理操作可以cankao https://docs.espressif.com/projects/esp-idf/zh_CN/latest/esp32/api-guides/wifi.html#esp32-wi-fi-station
    return 0;
}

int xcmd_wifi_disconnect(int argc, char* argv[])
{
    //ESP_ERROR_CHECK(esp_wifi_clear_default_wifi_driver_and_handlers(sta_netif));
    ESP_ERROR_CHECK(esp_wifi_disconnect());             //断开 Wi-Fi 连接
    ESP_ERROR_CHECK(esp_wifi_stop());                   //终止 Wi-Fi 驱动程序
    //ESP_ERROR_CHECK(esp_wifi_deinit());               //清理 Wi-Fi 驱动程序。
    return 0;
}


#define DEFAULT_SCAN_LIST_SIZE 20
static void print_auth_mode(int authmode)
{
    switch (authmode) {
    case WIFI_AUTH_OPEN:
        ESP_LOGI(TAG, "Authmode \tWIFI_AUTH_OPEN");
        break;
    case WIFI_AUTH_WEP:
        ESP_LOGI(TAG, "Authmode \tWIFI_AUTH_WEP");
        break;
    case WIFI_AUTH_WPA_PSK:
        ESP_LOGI(TAG, "Authmode \tWIFI_AUTH_WPA_PSK");
        break;
    case WIFI_AUTH_WPA2_PSK:
        ESP_LOGI(TAG, "Authmode \tWIFI_AUTH_WPA2_PSK");
        break;
    case WIFI_AUTH_WPA_WPA2_PSK:
        ESP_LOGI(TAG, "Authmode \tWIFI_AUTH_WPA_WPA2_PSK");
        break;
    case WIFI_AUTH_WPA2_ENTERPRISE:
        ESP_LOGI(TAG, "Authmode \tWIFI_AUTH_WPA2_ENTERPRISE");
        break;
    case WIFI_AUTH_WPA3_PSK:
        ESP_LOGI(TAG, "Authmode \tWIFI_AUTH_WPA3_PSK");
        break;
    case WIFI_AUTH_WPA2_WPA3_PSK:
        ESP_LOGI(TAG, "Authmode \tWIFI_AUTH_WPA2_WPA3_PSK");
        break;
    default:
        ESP_LOGI(TAG, "Authmode \tWIFI_AUTH_UNKNOWN");
        break;
    }
}

static void print_cipher_type(int pairwise_cipher, int group_cipher)
{
    switch (pairwise_cipher) 
    {
        case WIFI_CIPHER_TYPE_NONE:
            ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_NONE");
            break;
        case WIFI_CIPHER_TYPE_WEP40:
            ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_WEP40");
            break;
        case WIFI_CIPHER_TYPE_WEP104:
            ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_WEP104");
            break;
        case WIFI_CIPHER_TYPE_TKIP:
            ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_TKIP");
            break;
        case WIFI_CIPHER_TYPE_CCMP:
            ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_CCMP");
            break;
        case WIFI_CIPHER_TYPE_TKIP_CCMP:
            ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_TKIP_CCMP");
            break;
        default:
            ESP_LOGI(TAG, "Pairwise Cipher \tWIFI_CIPHER_TYPE_UNKNOWN");
            break;
    }

    switch (group_cipher) 
    {
        case WIFI_CIPHER_TYPE_NONE:
            ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_NONE");
            break;
        case WIFI_CIPHER_TYPE_WEP40:
            ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_WEP40");
            break;
        case WIFI_CIPHER_TYPE_WEP104:
            ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_WEP104");
            break;
        case WIFI_CIPHER_TYPE_TKIP:
            ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_TKIP");
            break;
        case WIFI_CIPHER_TYPE_CCMP:
            ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_CCMP");
            break;
        case WIFI_CIPHER_TYPE_TKIP_CCMP:
            ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_TKIP_CCMP");
            break;
        default:
            ESP_LOGI(TAG, "Group Cipher \tWIFI_CIPHER_TYPE_UNKNOWN");
            break;
    }
}

int xcmd_wifi_scan(int argc, char* argv[])
{
    unsigned short number = DEFAULT_SCAN_LIST_SIZE;                 // 默认扫描列表大小
    wifi_ap_record_t ap_info[DEFAULT_SCAN_LIST_SIZE];               // AP信息结构体大小
    unsigned short ap_count = 0;
    memset(ap_info, 0, sizeof(ap_info));            

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));              // 设置WiFi的工作模式为 STA
    ESP_ERROR_CHECK(esp_wifi_start());                              // 启动WiFi连接
    esp_wifi_scan_start(NULL, true);                                // 开始扫描WIFI(默认配置，阻塞方式)
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&number, ap_info));// 获取搜索的具体AP信息
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_num(&ap_count));           // 接入点的数量
    ESP_LOGI(TAG, "Total APs scanned = %u", ap_count);
    for (int i = 0; (i < DEFAULT_SCAN_LIST_SIZE) && (i < ap_count); i++) {  
        ESP_LOGI(TAG, "SSID \t\t%s", ap_info[i].ssid);              // 打印WIFI名称
        ESP_LOGI(TAG, "RSSI \t\t%d", ap_info[i].rssi);              // 打印信号强度	
        print_auth_mode(ap_info[i].authmode);                       // 打印认证方式
        if (ap_info[i].authmode != WIFI_AUTH_WEP) {
            print_cipher_type(ap_info[i].pairwise_cipher, ap_info[i].group_cipher);
        }
        ESP_LOGI(TAG, "Channel \t\t%d\n", ap_info[i].primary);
    }
    return 0;
}

int nvs_init(int argc, char* argv[])
{
    static unsigned char flag=0;
    if(flag == 1)   
    {   
        ESP_LOGD(TAG, "nvs flash already init success!");
        return 0;
    }
    flag = 1;
    // for wifi and only run once
    // Initialize NVS 
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK( ret );
    ESP_LOGD(TAG, "nvs flash init success!");
    return 0;
}


int xcmd_dev_reboot(int argc, char* argv[])
{
    unsigned int tim = 0;
    unsigned int i = 0;
    if( argc == 2) 
    {
        tim = atoi(argv[1]);
    }
    else
    {
        tim = 3;
    }
    for (; tim>0; tim--)
    {
        ESP_LOGE(TAG, "The device will restart in %d second", tim);
        vTaskDelay(1000 / portTICK_RATE_MS);
    }   
    esp_restart();
    return 0;
}

xcmd_t t_xcmd_pci[] = 
{
    {"nvs",         nvs_init,                   "nvs_init",                 NULL},    
    {"winit",       wifi_init,                  "wifi init",                NULL},    
    {"wcon",        xcmd_wifi_connect,          "xcmd_wifi_connect",        NULL},    
    {"wdisc",       xcmd_wifi_disconnect,       "xcmd_wifi_disconnect",     NULL},    
    {"wscan",       xcmd_wifi_scan,             "xcmd_wifi_scan",           NULL},    
    {"rtask",       xcmd_tsak_run,              "xcmd run task",            NULL},
    {"stask",       xcmd_tsak_suspend,          "xcmd suspend task",        NULL},
    {"dtask",       xcmd_tsak_delete,           "xcmd delete task",         NULL},
    {"ptask",       xcmd_tsak_info,             "xcmd display task info",   NULL},
    {"ltask",       xcmd_tsak_register_info,    "Lists the tasks that can be run", NULL},
    {"reboot",      xcmd_dev_reboot,            "device reboot",            NULL}
    
};


void xcmd_esp_init(void)
{
    xcmd_cmd_register(t_xcmd_pci, sizeof(t_xcmd_pci)/sizeof(xcmd_t));
}

int cmd_get_char(unsigned char *ch)
{
    *ch = getc(stdin);
    return 1;
}

int cmd_put_char(unsigned char ch)
{
    putchar(ch);
    return 1;
}

void xcmd_main(void *pvParameter) 
{
	while(1)
	{
        xcmd_task();
        vTaskDelay(1); 
	}
}


t_xcmd_task xcmd_main_task = {
	.p_task_desc.task_entry = xcmd_main,
	.p_task_desc.task_name = "xcmd_task",
	.p_task_desc.stackdepth = 1024*16,
	.p_task_desc.priority = 6,
};

t_xcmd_task eventsrv_main_task = {
	.p_task_desc.task_entry = eventsrv_task,
	.p_task_desc.task_name = "eventsrv",
	.p_task_desc.stackdepth = 1024*8,
	.p_task_desc.priority = 7,
};


void esp_xcmd_init(void)
{
	ESP_LOGI(TAG, "Create Xcmd App Start......");
    xcmd_init(cmd_get_char, cmd_put_char);
    xcmd_task_init();
    xcmd_esp_init();
    xcmd_tsak_register(&xcmd_main_task, 1);
    xcmd_tsak_register(&eventsrv_main_task, 1);	
}