#include "eventsrv.h"
#include "xcmd_task.h"
#include "xcmd.h"
#include "freertos/queue.h"

typedef struct 
{
   unsigned int server_id;
   unsigned int event_type;
   void *signal_ptr;
}t_sever_info;

typedef struct 
{
    unsigned char  isvalid;
    REG_CALLBACK   default_fun;
    unsigned char  total_event_num;
    unsigned int   clinet_num;
    t_eventclient_list clinet_list;
}t_eventsrv_lsit;

t_eventsrv_lsit g_eventsrv[MAX_SERVICE];

static unsigned int clint_id_num = 0;

#define EVENTSRV_QUEUE_LEN  40
#define EVENTSRV_ITEM_SIZE  sizeof(t_sever_info)
static QueueSetHandle_t eventsrv_que;

int xcmd_createsrv_eventlist(unsigned int server_id, unsigned char total_event_num, REG_CALLBACK default_fun)
{
    if (server_id > MAX_SERVICE)
    {
        xcmd_print("The service is invalid, server_id: %d, max id\r\n", server_id, MAX_SERVICE);
        return -1;
    }
    if (g_eventsrv[server_id].isvalid == 0)
    {
        g_eventsrv[server_id].isvalid = 1;
        g_eventsrv[server_id].total_event_num = total_event_num;
        g_eventsrv[server_id].default_fun = default_fun;
        return 0;
    }
    else
    {
        xcmd_print("Service not enabled,  server_id: %d\r\n", server_id);
        return -1;
    }
}

int xcmd_deletesrv_eventlist(unsigned int  server_id)
{
    if (server_id > MAX_SERVICE)
    {
        xcmd_print("The service is invalid, server_id: %d, max id\r\n", server_id, MAX_SERVICE);
        return -1;
    }
    g_eventsrv[server_id].isvalid = 0;
    return 0;
}


int xcmd_register_client(unsigned int server_id, t_eventclient_list  *client_opt)
{
    if (server_id > MAX_SERVICE)
    {
        xcmd_print("The service is invalid, server_id: %d, max id\r\n", server_id, MAX_SERVICE);
        return -1;
    }
    client_opt->client_id = clint_id_num;
    g_eventsrv[server_id].clinet_num++;
    clint_id_num++;
    list_add(&g_eventsrv[server_id].clinet_list.eventclient_list, &client_opt->eventclient_list);
    return 0;
}

int xcmd_unregister_client(unsigned int server_id, t_eventclient_list  *client_opt)       
{
    struct list_head *pos;
    t_eventclient_list *pclient;

    if (server_id > MAX_SERVICE)
    {
        xcmd_print("The service is invalid, server_id: %d, max id\r\n", server_id, MAX_SERVICE);
        return -1;
    }
    list_for_each(pos, &g_eventsrv[server_id].clinet_list.eventclient_list)
    {
        if(client_opt->client_id == ((t_eventclient_list*)pos)->client_id)
        {
            pclient = (t_eventclient_list*)pos;
            list_del(&pclient->eventclient_list);
            g_eventsrv[server_id].clinet_num--;
        }
    }
    return 0;
}

int xcmd_isclientregisted(unsigned int server_id, t_eventclient_list  *client_opt)
{
    struct list_head *pos;

    if (server_id > MAX_SERVICE)
    {
        xcmd_print("The service is invalid, server_id: %d, max id\r\n", server_id, MAX_SERVICE);
        return -1;
    }
    if (g_eventsrv[server_id].isvalid == 0)
    {
        xcmd_print("Service not enabled,  server_id: %d\r\n", server_id);
        return -1;
    }
    list_for_each(pos, &g_eventsrv[server_id].clinet_list.eventclient_list)
    {
        if(client_opt->client_id == ((t_eventclient_list*)pos)->client_id)
        {
            return 0;
        }
    }
    xcmd_print("client is not registered. %d\r\n", client_opt->client_id);
    return  -1;
}

int xcmd_sendeventtoclient(unsigned int server_id, unsigned int event_type, void *signal_ptr)
{
    t_sever_info server_info;
    if (server_id > MAX_SERVICE)
    {
        xcmd_print("The service is invalid, server_id: %d, max id\r\n", server_id, MAX_SERVICE);
        return -1;
    }
    if (g_eventsrv[server_id].isvalid == 0)
    {
        xcmd_print("Service not enabled,  server_id: %d\r\n", server_id);
        return -1;
    }
    if (event_type > g_eventsrv[server_id].total_event_num)
    {
        xcmd_print("The service is invalid, server_id: %d, max id\r\n", server_id, MAX_SERVICE);
        return -1;
    }
    server_info.event_type = event_type;
    server_info.server_id = server_id;
    server_info.signal_ptr = signal_ptr;
    if (xQueueSend(eventsrv_que, &server_info, 10 / portTICK_RATE_MS) != true) 
    {
        return -1;
    }
    return 0;
}

void eventsrv_task(void *arg)
{   
    t_sever_info server_info;
    struct list_head *pos;
    unsigned int i = 0;

    for (i=0; i<MAX_SERVICE; i++)
    {
        g_eventsrv[i].clinet_num = 0;
        g_eventsrv[i].isvalid = 0;
        INIT_LIST_HEAD(&g_eventsrv[i].clinet_list.eventclient_list);
    }

    eventsrv_que = xQueueCreate(EVENTSRV_QUEUE_LEN, EVENTSRV_ITEM_SIZE);
    while(1)
    {
        if (xQueueReceive(eventsrv_que, (void*)&server_info, portMAX_DELAY) == true)
        {
            xcmd_print("server_id = %d, event_type = %d\r\n", server_info.server_id , server_info.event_type);
            if(g_eventsrv[server_info.server_id].clinet_num == 0) 
            {
                xcmd_print("clinet_num = 0, so run def fun \r\n");
                g_eventsrv[server_info.server_id].default_fun(server_info.server_id, server_info.event_type, server_info.signal_ptr);
                continue;
            }
            list_for_each(pos, &g_eventsrv[server_info.server_id].clinet_list.eventclient_list)
            { 
                if(server_info.event_type > ((t_eventclient_list*)pos)->start_event && \
                        server_info.event_type < ((t_eventclient_list*)pos)->end_event)
                {
                    ((t_eventclient_list*)pos)->event_fun(server_info.server_id, server_info.event_type, \
                            server_info.signal_ptr);  
                }
            }
        } 

    }
}   



void test_def_fun(unsigned int id, unsigned int argc, void *argv)
{
    xcmd_print("id = %d, event_type = %d \r\n", id, argc);
}

void xcmd_eventsrv_init(void)
{
    int ret = 0;
    
    xcmd_createsrv_eventlist(KEYPAD_SERVICE, 5, test_def_fun);
}
