#ifndef EVENTSRV_H
#define EVENTSRV_H
#include "list.h"
#ifdef __cplusplus
extern   "C" {
#endif


typedef enum
{
    SYSTREAM_SERVICE,
    KEYPAD_SERVICE,
    WIFI_SERVICE,
    MAX_SERVICE
} SRV_ID_E;


typedef void (* REG_CALLBACK) (unsigned int id, unsigned int argc, void *argv);

typedef struct 
{
    struct list_head eventclient_list;
    unsigned int start_event;
    unsigned int end_event;
    unsigned int client_id;
    REG_CALLBACK event_fun;
}t_eventclient_list;


int xcmd_createsrv_eventlist(unsigned int server_id, unsigned char total_event_num, REG_CALLBACK default_fun);

int xcmd_deletesrv_eventlist(unsigned int  server_id);

int xcmd_register_client(unsigned int server_id, t_eventclient_list  *client_opt);

int xcmd_unregister_client(unsigned int server_id, t_eventclient_list  *client_opt);

int xcmd_isclientregisted(unsigned int server_id, t_eventclient_list  *client_opt);

int xcmd_sendeventtoclient(unsigned int server_id, unsigned int event_type, void *signal_ptr);

void eventsrv_task(void *arg);

#ifdef __cplusplus
        }
#endif


#endif