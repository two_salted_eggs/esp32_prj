#include "xcmd_task.h"
#include "list.h"
#include "xcmd.h"

static t_xcmd_task run_tsak;
static t_xcmd_task sus_tsak;
static t_xcmd_task idle_tsak;

void xcmd_task_init(void)
{
    INIT_LIST_HEAD(&run_tsak.task_list);
    INIT_LIST_HEAD(&sus_tsak.task_list);
    INIT_LIST_HEAD(&idle_tsak.task_list);
}

void xcmd_tsak_register(t_xcmd_task *ptask, uint8_t status)
{
    if(status)
    {
        xTaskCreate(ptask->p_task_desc.task_entry,\
					ptask->p_task_desc.task_name,\
					ptask->p_task_desc.stackdepth,\
					ptask->p_task_desc.parameters,
					ptask->p_task_desc.priority,\
					&ptask->p_task_desc.pvcreatedtask);
        list_add(&ptask->task_list, &run_tsak.task_list);
    }
    else
    {
        list_add(&ptask->task_list, &idle_tsak.task_list);
    }
    return;
}

int xcmd_tsak_register_info(int argc, char* argv[])
{
    struct list_head *pos;
    xcmd_print("--------------RUNNING TASK-----------------\r\n");
    xcmd_print("%-20s %-20s %-20s\r\n", "task_name", "stackdepth", "priority");
    list_for_each(pos, &run_tsak.task_list) {
        xcmd_print("%-20s %-20d %-20d\r\n", ((t_xcmd_task*)pos)->p_task_desc.task_name, \
                    ((t_xcmd_task*)pos)->p_task_desc.stackdepth, \
                    ((t_xcmd_task*)pos)->p_task_desc.priority);
    }
    xcmd_print("\r\n");
    xcmd_print("--------------SUSPEND TASK-----------------\r\n");
    xcmd_print("%-20s %-20s %-20s\r\n", "task_name", "stackdepth", "priority");
    list_for_each(pos, &sus_tsak.task_list) {
        xcmd_print("%-20s %-20d %-20d\r\n", ((t_xcmd_task*)pos)->p_task_desc.task_name, \
                    ((t_xcmd_task*)pos)->p_task_desc.stackdepth, \
                    ((t_xcmd_task*)pos)->p_task_desc.priority);
    }
    xcmd_print("\r\n");
    xcmd_print("--------------IDLE TASK-----------------\r\n");
    xcmd_print("%-20s %-20s %-20s\r\n", "task_name", "stackdepth", "priority");
    list_for_each(pos, &idle_tsak.task_list) {
        xcmd_print("%-20s %-20d %-20d\r\n", ((t_xcmd_task*)pos)->p_task_desc.task_name, \
                    ((t_xcmd_task*)pos)->p_task_desc.stackdepth, \
                    ((t_xcmd_task*)pos)->p_task_desc.priority);
    }
    xcmd_print("\r\n");
    return 0;
}

//运行任务
int xcmd_tsak_run(int argc, char* argv[])
{
    struct list_head *pos;
    char *str = NULL;
    t_xcmd_task *ptask;

    if (argc == 2) {
        str = argv[1];
    }
    else {
        xcmd_print("error\r\n");
        return -1;
    }

    list_for_each(pos, &idle_tsak.task_list) {
        if(strcmp(((t_xcmd_task*)pos)->p_task_desc.task_name, str) == 0)
        {
            xTaskCreate(((t_xcmd_task*)pos)->p_task_desc.task_entry,\
					((t_xcmd_task*)pos)->p_task_desc.task_name,\
					((t_xcmd_task*)pos)->p_task_desc.stackdepth,\
					((t_xcmd_task*)pos)->p_task_desc.parameters,
					((t_xcmd_task*)pos)->p_task_desc.priority,\
					&((t_xcmd_task*)pos)->p_task_desc.pvcreatedtask);
            ptask = (t_xcmd_task*)pos;
            list_del(&ptask->task_list);
            list_add(&ptask->task_list, &run_tsak.task_list);
            xcmd_print("run idle task success\r\n");
            return 0;
        }
    }
    list_for_each(pos, &sus_tsak.task_list) {
        if(strcmp(((t_xcmd_task*)pos)->p_task_desc.task_name, str) == 0)
        {
            vTaskResume(((t_xcmd_task*)pos)->p_task_desc.pvcreatedtask);
            ptask = (t_xcmd_task*)pos;
            list_del(&ptask->task_list);
            list_add(&ptask->task_list, &run_tsak.task_list);
            xcmd_print("run suspend task success\r\n");
            return 0;
        }
    }
    return 0;  
}

//挂起任务
int xcmd_tsak_suspend(int argc, char* argv[])
{
    struct list_head *pos;
    char *str = NULL;
    t_xcmd_task *ptask;

    if (argc == 2) {
        str = argv[1];
    }
    else {
        xcmd_print("error\r\n");
        return -1;
    }

    list_for_each(pos, &run_tsak.task_list) {
        if(strcmp(((t_xcmd_task*)pos)->p_task_desc.task_name, str) == 0)
        {
            xcmd_print("pvcreatedtask %x\r\n",((t_xcmd_task*)pos)->p_task_desc.pvcreatedtask);
            vTaskSuspend(((t_xcmd_task*)pos)->p_task_desc.pvcreatedtask);
            ptask = (t_xcmd_task*)pos;
            list_del(&ptask->task_list);
            list_add(&ptask->task_list, &sus_tsak.task_list);
            xcmd_print("suspend task success\r\n");
            return 0;
        }
    }
    return -1;
}

//删除任务
int xcmd_tsak_delete(int argc, char* argv[])
{
    struct list_head *pos;
    char *str = NULL;
    t_xcmd_task *ptask;

    if (argc == 2) {
        str = argv[1];
    }
    else {
        xcmd_print("error\r\n");
        return -1;
    }

    list_for_each(pos, &run_tsak.task_list) {
        if(strcmp(((t_xcmd_task*)pos)->p_task_desc.task_name, str) == 0)
        {
            vTaskDelete(((t_xcmd_task*)pos)->p_task_desc.pvcreatedtask);
            ptask = (t_xcmd_task*)pos;
            list_del(&ptask->task_list);
            list_add(&ptask->task_list, &idle_tsak.task_list);
            xcmd_print("del running task success\r\n");
            return 0;
        }
    }
    list_for_each(pos, &sus_tsak.task_list) {
        if(strcmp(((t_xcmd_task*)pos)->p_task_desc.task_name, str) == 0)
        {
            vTaskDelete(((t_xcmd_task*)pos)->p_task_desc.pvcreatedtask);
            ptask = (t_xcmd_task*)pos;
            list_del(&ptask->task_list);
            list_add(&ptask->task_list, &idle_tsak.task_list);
            xcmd_print("del suspend task success\r\n");
            return 0;
        }
    }
    return -1;
}

//显示任务信息
int xcmd_tsak_info(int argc, char* argv[])
{
    static char str[2048];
    //xcmd_main_task  X       8       6272    12      -1
    printf("\t%s\t\t%s\t\t%s\t%s\t%s\t%s\r\n", "task name", "status", "priority", "free_stack", "ID", "CoreID");
    printf("---------------------------------------------------\r\n");
    vTaskList(str);
    printf(str);
    printf("---------------------------------------------------\r\n");
    return 0;
}