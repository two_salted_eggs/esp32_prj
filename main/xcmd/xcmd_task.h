#ifndef __XCMD_TASK_H__
#define __XCMD_TASK_H__

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "list.h"

typedef struct _task_desc {
	TaskFunction_t                  task_entry;
    char *                          task_name;
    unsigned int                    stackdepth;
    void *                          parameters;
    UBaseType_t                     priority;
    TaskHandle_t                    pvcreatedtask;
}t_task_desc;

typedef struct 
{
    struct list_head task_list;
    t_task_desc p_task_desc;
}t_xcmd_task;


void xcmd_task_init(void);

/**
 * register a new task and whether to run immediately
 * 
 * @param ptask 
 *
 * @param status 
 *
 * @return 
 *
 * @note 
 */
void xcmd_tsak_register(t_xcmd_task *ptask, uint8_t status);

/**
 * Displays tasks that have been run
 * 
 * @param ptask 
 *
 * @param status 
 *
 * @return 
 *
 * @note 
 */
int xcmd_tsak_register_info(int argc, char* argv[]);

//运行任务
int xcmd_tsak_run(int argc, char* argv[]);

//挂起任务
int xcmd_tsak_suspend(int argc, char* argv[]);

//删除任务
int xcmd_tsak_delete(int argc, char* argv[]);

//显示任务信息
int xcmd_tsak_info(int argc, char* argv[]);

#endif
